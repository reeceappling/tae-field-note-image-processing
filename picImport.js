const args = process.argv;

const fs = require('fs');
fs.readdir(args[2], (err, files) => {
	var counter=0;
	var temp=3;
	files.forEach(file => {
		if(file.split('.').pop()=="JPG"){
			counter++;
			var leadingzeros="";
			if(counter<10){temp=2;}
			else if(counter>9&&counter<100){temp=1;}
			else{temp=0;}
			for(var i=0;i<temp;i++){leadingzeros=leadingzeros+"0";}
			fs.rename(args[2]+'\\'+file, args[2]+'\\'+args[2].split("\\").pop().substr(10)+"_"+leadingzeros+counter+".JPG", function(err) {
				if ( err ) console.log('ERROR: ' + err);
			});
		}
	});
})